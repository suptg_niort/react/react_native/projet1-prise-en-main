import React from 'react';
import { StyleSheet, Button, View, TextInput } from 'react-native';

class Search extends React.Component {
  render() {
    return (
            <View style={{flex:1, justifyContent:'center', flexDirection:'row', backgroundColor:'yellow'}}>
              <View style={{height: 50, width:50, backgroundColor: 'red'}}></View>
              <View style={{height: 50, width:50,  backgroundColor: 'green'}}></View>
            </View>
            );
  }
}

const styles= StyleSheet.create( {
  textinput: {
    marginLeft:5,
    marginRight:5,
    height:50,
    borderColor: '#000',
    borderWidth:1,
    paddingLeft:5
  }
});

export default Search;